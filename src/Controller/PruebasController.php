<?php

/**
* Controlador del módulo Pruebas
*/

// Declarar namespace
namespace Drupal\pruebas\Controller;

// Importar clases
use Drupal\Core\Controller\ControllerBase; // Controlador base
use Drupal\Core\Url; // Clase que permite recuperar url's basándose en el sistema de routing de D8


// Declaración de la clase
class PruebasController extends ControllerBase
{
	// Función de descripcion
	public function descripcion() 
	{
		// Crear url a partir de una ruta (del routing.yml del módulo block)
		$url = Url::fromRoute( 'block.admin_display' );
		
		// Crear el link
		$enlace = $this -> l( $this -> t( 'Página de administración de bloques' ), $url );
		
		// Declarar arreglo con el contenido
		$build = array(
			'#type' => 'markup',
			'#markup' => '<p>Hola drupaleros</p><p>' . $enlace . '</p>'
		);
		
		// Devolver arreglo del contenido
		return $build;
	}
	
	
	
	// Función para prueba con parámetros
	public function parametros( $parametro1, $parametro2 )
	{
		// Declarar arreglo con el contenido
		$build = array(
			'#type' => 'markup',
			'#markup' => '<p>Los parámetros que llegaron son Parámetro 1: <b>' . $parametro1 . '</b> y Parámetro 2: <b> ' . $parametro2 . ' </b></p>'
		);
		
		// Devolver arreglo del contenido
		return $build;
	}
	
	
	
	// Función para prueba con parámetros enteros
	public function parametros_enteros( $parametro1, $parametro2 )
	{
		// Declarar arreglo con el contenido
		$build = array(
			'#type' => 'markup',
			'#markup' => '<p>Los parámetros enteros que llegaron son Parámetro 1: <b>' . $parametro1 . '</b> y Parámetro 2: <b> ' . $parametro2 . ' </b></p>'
		);
		
		// Devolver arreglo del contenido
		return $build;
	}
	
	
	
	// Función para ruta con parámetros opcionales
	public function parametros_opcionales( $parametro1, $parametro2 )
	{
		// Inicializar cadena a retornar
		$contenido = '';
		
		// Si ambos parámetros son NULL
		if( $parametro1 == NULL && $parametro2 == NULL ) {
			$contenido = '<p>No llegó ningún parámetro<p>';
		}
		
		// En caso contrario
		else
		{
			// Si el parámetro 1 es diferente de NULL
			if( $parametro1 != NULL ) {
				$contenido .= '<p>Parámetro 1: <strong>' . $parametro1 . '</strong></p>';
			}
			// Si el parámetro 2 es diferente de NULL
			if( $parametro2 != NULL ) {
				$contenido .= '<p>Parámetro 2: <strong>' . $parametro2 . '</strong></p>';
			}
		}
		
		// Declarar arreglo con el contenido
		$build = array(
			'#type' => 'markup',
			'#markup' => $contenido,
		);
		
		// Devolver arreglo del contenido
		return $build;
	}
	
	
	
	// Función para prueba con template
	public function template()
	{
		// Declarar arreglo con el contenido
		$build = array(
			'#cache' => array( 'max-age' => 0 ), // Con esta línea ya no se guarda la caché
			'#theme' => 'template1',
			'#var_template1' => '10',
			'#var_template2' => '20',
			'#arreglo' => array(
				'1' => 'Uno',
				'2' => 'Dos',
				'3' => 'Tres',
				'4' => 'Cuatro',
				'9' => 'Nueve',
			),
		);
		
		// Devolver arreglo del contenido
		return $build;
	}
	
	
	
	// Función para prueba con formulario
	public function formulario()
	{
		// Cargar formulario
		$formulario = \Drupal::formBuilder() -> getForm( 'Drupal\pruebas\Form\PruebasForm' );
		
		// Declarar arreglo con el contenido
		$build = array(
			'#cache' => array( 'max-age' => 0 ), // Con esta línea ya no se guarda la caché
			'#theme' => 'template_formulario',
			'#form' => $formulario,
		);
		
		// Devolver arreglo con el contenido
		return $build;
	}
	
	
	
	// Función para prueba de incluir un template dentro de otro
	public function template_include()
	{
		// Declarar arreglo con el contenido
		$build = array(
			'#cache' => array( 'max-age' => 0 ), // Con esta línea ya no se guarda la caché
			'#theme' => 'template_include',
			'#var_template1' => '10',
		);
		
		// Devolver arreglo con el contenido
		return $build;
	}
	
	
	
	// Función para prueba de hacer consultas y otras operaciones en la base de datos
	public function consultas()
	{
		// Inicializar variables a retornar
		$nombre_usuario = '';
		$total_nodos = 0;
		$arreglo_select = array();
		$arreglo_join = array();
		$arreglo_limit = array();
		$arreglo_uno = array();
		$arreglo_antes = array();
		$arreglo_despues = array();
		
		
		
		// Hacer consulta para cargar usuarios. Se carga el usuario actual.
		// Cargar usuario
		$usuario = \Drupal\user\Entity\User::load( \Drupal::currentUser() -> id() );
		
		// Si se encontró al usuario
		if( isset( $usuario ) && $usuario != NULL ) {
			// Dar valor a la variable del nombre
			$nombre_usuario = $usuario -> getUsername();
		}
		
		
		
		// Query con COUNT
		// Hacer consulta para contar el número de nodos del sistema
		$query = \Drupal::database() -> select( 'node', 'n' );
		$query -> addExpression( 'COUNT( nid )' );
		$query -> condition( 'nid', '0', '>' );
		
		// Guardar el resultado de la consulta
		$total_nodos = $query -> execute() -> fetchField();
		
		
		
		// Query con SELECT
		// Hacer consulta para obtener todos los nid's y tipos de los nodos de Drupal
		$query = \Drupal::database() -> select( 'node', 'n' )
			-> fields( 'n', array( 'nid', 'type' ) )
			-> condition( 'nid', '0', '>' )
			-> orderBy( 'nid', 'ASC' )
			-> execute();
		
		// Obtener resultados
		$results = $query -> fetchAll();
		
		// Inicializar contador
		$cont = 0;
		
		// Recorrer resultados
		foreach( $results as $record )
		{
			// Asignar valores al arreglo
			$arreglo_select[ $cont ][ 'nid' ] = $record -> nid;
			$arreglo_select[ $cont ][ 'type' ] = $record -> type;
			
			// Incrementar contador
			$cont++;
		}
		
		
		
		// Query con SELECT y JOIN
		$query = \Drupal::database() -> select( 'node', 'n' );
		$query -> join( 'node_field_data', 'nd', 'nd.nid = n.nid' );
		$query -> fields( 'n', array( 'nid', 'type' ) )
			-> fields( 'nd', array( 'title' ) )
			-> condition( 'uid', '1', '=' )
			-> orderBy( 'nid', 'ASC' );
		
		// Obtener resultados
		$results = $query -> execute() -> fetchAll();
		
		// Inicializar contador
		$cont = 0;
		
		// Recorrer resultados
		foreach( $results as $record )
		{
			// Asignar valores al arreglo
			$arreglo_join[ $cont ][ 'nid' ] = $record -> nid;
			$arreglo_join[ $cont ][ 'type' ] = $record -> type;
			$arreglo_join[ $cont ][ 'title' ] = $record -> title;
			
			// Incrementar contador
			$cont++;
		}
		
		
		
		// Query con SELECT y LIMIT (RANGE)
		// Hacer consulta para obtener los dos primeros nodos creados usando LIMIT
		$query = \Drupal::database() -> select( 'node_field_data', 'nd' )
			-> fields( 'nd', array( 'nid', 'title' ) )
			-> condition( 'uid', '1', '=' )
			-> orderBy( 'nid', 'ASC' )
			-> range( 0, 2 )
			-> execute();
		
		// Obtener resultados
		$results = $query -> fetchAll();
		
		// Inicializar contador
		$cont = 0;
		
		// Recorrer resultados
		foreach( $results as $record )
		{
			// Asignar valores al arreglo
			$arreglo_limit[ $cont ][ 'nid' ] = $record -> nid;
			$arreglo_limit[ $cont ][ 'title' ] = $record -> title;
			
			// Incrementar contador
			$cont++;
		}
		
		
		
		// Query con SELECT para UN SOLO REGISTRO
		// Hacer consulta para obtener el último nodo registrado
		$query = \Drupal::database() -> select( 'node_field_data', 'nd' )
			-> fields( 'nd', array( 'nid', 'title' ) )
			-> orderBy( 'nid', 'DESC' )
			-> range( 0, 1 )
			-> execute();
		
		// Obtener ese único registro
		$record = $query -> fetchObject();
		
		// Si hubo resultados en la consulta
		if( isset( $record ) && $record != NULL ) {
			$arreglo_uno[ 'nid' ] = $record -> nid;
			$arreglo_uno[ 'title' ] = $record -> title;
		}
		
		
		
		// Query con INSERT
		// Hacer inserciones en la tabla de PRUEBAS
		$query = \Drupal::database() -> insert( 'pruebas' )
			-> fields( array(
				'texto' => 'Texto de prueba 1',
				'numero' => 10,
			) )
			-> execute();
		$query = \Drupal::database() -> insert( 'pruebas' )
			-> fields( array(
				'texto' => 'Texto de prueba 2',
				'numero' => 20,
			) )
			-> execute();
		$query = \Drupal::database() -> insert( 'pruebas' )
			-> fields( array(
				'texto' => 'Texto de prueba 3',
				'numero' => 30,
			) )
			-> execute();
		
		
		
		// Hacer SELECT antes de actualizar
		$query = \Drupal::database() -> select( 'pruebas', 'p' )
			-> fields( 'p', array( 'id', 'texto', 'numero' ) )
			-> orderBy( 'id', 'ASC' )
			-> execute();
		
		// Obtener resultados
		$results = $query -> fetchAll();
		
		// Inicializar contador
		$cont = 0;
		
		// Recorrer resultados
		foreach( $results as $record )
		{
			// Asignar valores al arreglo
			$arreglo_antes[ $cont ][ 'id' ] = $record -> id;
			$arreglo_antes[ $cont ][ 'texto' ] = $record -> texto;
			$arreglo_antes[ $cont ][ 'numero' ] = $record -> numero;
			
			// Incrementar contador
			$cont++;
		}
		
		
		
		// Query con UPDATE
		// Hacer actualizaciones en la tabla de PRUEBAS
		$query = \Drupal::database() -> update( 'pruebas' )
			-> fields( array(
				'texto' => 'Texto de prueba 1.1',
				'numero' => 11,
			) )
			-> condition( 'numero', 10, '=' )
			-> execute();
		$query = \Drupal::database() -> update( 'pruebas' )
			-> fields( array(
				'texto' => 'Texto de prueba 2.2',
				'numero' => 22,
			) )
			-> condition( 'numero', 20, '=' )
			-> execute();
		$query = \Drupal::database() -> update( 'pruebas' )
			-> fields( array(
				'texto' => 'Texto de prueba 3.3',
				'numero' => 33,
			) )
			-> condition( 'numero', 30, '=' )
			-> execute();
		
		
		
		// Hacer SELECT después de actualizar
		$query = \Drupal::database() -> select( 'pruebas', 'p' )
			-> fields( 'p', array( 'id', 'texto', 'numero' ) )
			-> orderBy( 'id', 'ASC' )
			-> execute();
		
		// Obtener resultados
		$results = $query -> fetchAll();
		
		// Inicializar contador
		$cont = 0;
		
		// Recorrer resultados
		foreach( $results as $record )
		{
			// Asignar valores al arreglo
			$arreglo_despues[ $cont ][ 'id' ] = $record -> id;
			$arreglo_despues[ $cont ][ 'texto' ] = $record -> texto;
			$arreglo_despues[ $cont ][ 'numero' ] = $record -> numero;
			
			// Incrementar contador
			$cont++;
		}
		
		
		
		// Query con DELETE
		$query = \Drupal::database() -> delete( 'pruebas' )
			-> execute();
		
		
		
		
		// Declarar arreglo con el contenido
		$build = array(
			'#cache' => array( 'max-age' => 0 ), // Con esta línea ya no se guarda la caché
			'#theme' => 'template_consultas',
			'#nombre_usuario' => $nombre_usuario,
			'#total_nodos' => $total_nodos,
			'#arreglo_select' => $arreglo_select,
			'#arreglo_join' => $arreglo_join,
			'#arreglo_limit' => $arreglo_limit,
			'#arreglo_uno' => $arreglo_uno,
			'#arreglo_antes' => $arreglo_antes,
			'#arreglo_despues' => $arreglo_despues,
		);
		
		// Devolver arreglo con el contenido
		return $build;
	}

}

?>