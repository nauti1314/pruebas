<?php

/**
* Clase para el formulario de pruebas
*
* Si se pretende recuperar el formulario, se puede usar:
* $form = \Drupal::formBuilder() -> getForm( 'Drupal\pruebas\Form\PruebasForm' );
*/

// Declarar namespace
namespace Drupal\pruebas\Form;

// Importar clases
use Drupal\Core\Form\FormBase; // Clase base para formularios
use Drupal\Core\Form\FormStateInterface; // Interfaz que contiene el estado actual de un formulario


// Declaración de clase
class PruebasForm extends FormBase
{
	/**
	* Función que obtiene el id del formulario
	*
	* {@inheritdoc}
	*
	*/
	public function getFormId()
	{
		// Retornar el id del formulario
		return 'pruebas_form';
	}
	
	
	
	/**
	* Función que construye el formulario
	*
	* {@inheritdoc}
	*
	*/
	public function buildForm( array $form, FormStateInterface $form_state )
	{
		// Crear campo de texto
		$form[ 'texto_1' ] = array(
			'#type' => 'textfield',
			'#title' => 'Campo de texto',
			'#default_value' => 'Valor default',
			'#size' => 50,
			'#mexlength' => 50,
			'#required' => TRUE,
		);
		
		// Campo de selección
		$form[ 'select_1' ] = array(
			'#type' => 'select',
			'#title' => 'Campo de selección',
			'#options' => [
				'1' => 'Uno',
				'2' => 'Dos',
				'3' => 'Tres',
			],
		);
		
		// Campos de número
		$form[ 'numero_1' ] = array(
			'#type' => 'number',
			'#title' => 'Campo de número',
		);
		
		// Botón submit
		$form[ 'submit_1' ] = array(
			'#type' => 'submit',
			'#value' => 'Enviar',
		);
		
		// Retornar formulario
		return $form;
	}
	
	
	
	/**
	* Función para validar el formulario
	*
	* {@inheritdoc}
	*
	*/
	public function validateForm( array &$form, FormStateInterface $form_state )
	{
		// Validar que la longitud del texto sea mayor a tres
		if( strlen( $form_state -> getValue( 'texto_1' ) ) < 3 ) {
			// Marcar error
			$form_state -> setErrorByName( 'texto_1', 'El campo de texto debe de ser mayor a 3 caracteres.' );
		}
	}
	
	
	
	/**
	* Función para el envío del formulario
	*  
	* {@inheritdoc}
	*
	*/
	public function submitForm( array &$form, FormStateInterface $form_state )
	{
		// Crear cadena
		$cadena = 'Los valores enviados son: ' . $form_state -> getValue( 'texto_1' ) .
				' - ' . $form_state -> getValue( 'select_1' ) . ' - ' . $form_state -> getValue( 'numero_1' );
		
		// Enviar mensaje
		drupal_set_message( $cadena, 'status' );
	}
}

?>